import { create } from "zustand";

const useRegisterBookingReview = create((set) => ({
    currentRating: 0,
    categoryRating: [],
    bookings: [],
    booking: {},
    bookingReview: {},
    setCategoryRating: (newCategoryRating) => set({categoryRating: newCategoryRating}),
    setCurrentRating: (newCurrentRating) => set({ currentRating: newCurrentRating }),
    setBookings: (newBookings) => set({ bookings: newBookings }),
    setBooking: (newBooking) => set({ booking: newBooking }),
    setBookingReview: (newBookingReview) => set({ bookingReview: newBookingReview }),
}))

export default useRegisterBookingReview;