import { get } from "../../utils/request";

export const getHotelById = async (data) => {
  const response = await get(`/public/v1/hotel/get-condition/id`, {
    params: data,
  });
  return response.data;
};

export const getHotelAndCategoryById = async (id) => {
  const response = await get(`public/v1/hotel/get-hotel-and-category?hotelId=${id}`);
  return response.data;
}
