import { get } from "../../../utils/request";


export const getHotelReviews = async (token) => {
    const response = await get(`/member/v1/hotel/get-all-review-by-user`, {
        params: {
            currentPage: 1,
            limitPage: 8,
            status: 'ACTIVE'
        }
    },token);
    return response;
};