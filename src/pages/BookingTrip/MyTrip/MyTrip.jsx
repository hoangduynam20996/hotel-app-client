import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { getBooking } from "../../../api/User/Booking";
import { UserContext } from "../../../components/Contexts/AppUserProvider";
import Image from "../../../components/Image/Image";
import Title from "../../../components/Title/Title";
import ItemTrip from './ItemTrip';
import MyTripSkeleton from "./MyTripSkeleton";

function MyTrip() {
    const { t } = useTranslation();
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const {user, userLoading,token} = useContext(UserContext);

    useEffect(() => {
        if(!userLoading && Object.keys(user).length > 0 && token){
            const fetchData = async() => {
                try {
                    setLoading(true)
                    const results = await getBooking(token)
                    setData(results.data)
                    setLoading(false)
                } catch (error) {
                    setData([])
                    setLoading(false)
                }
            }
            fetchData()
        }
    },[user,userLoading,token])

    return ( 
        <div className="flex w-full">
            <div className="flex flex-col gap-12 w-full">
                <Title title={t("MyTrip.title")} fontBold extraLarge9 />

                <div className="w-full">
                    {
                        loading ? (
                            <div className="flex flex-col gap-4">
                                {Array.from({length: 1}).map((_,index) => (
                                    <div key={index}>
                                        <MyTripSkeleton />
                                    </div>
                                ))}
                            </div>
                        ) : (
                            <>
                                <div className="flex flex-col gap-4 w-full">
                                    {data && data.length > 0 ? 
                                    <>
                                        {data.map((item, index) => (
                                            <div key={index}>
                                                <ItemTrip item={item}/>
                                            </div>
                                        ))}
                                    </>
                                    : <div className="w-full flex flex-col items-center justify-center text-center">
                                        <div className="w-[160px] h-[210px] 2md:w-[210px] 2md:h-[260px]">
                                            <Image src="/images/world.png"/>
                                        </div>
                                        <Title title={t("MyTrip.yourTrip")} fontBold extraLarge9 nowrap={false}/>
                                        <Title title={t("MyTrip.showAll")} xxl nowrap={false}/>
                                      </div>
                                    }
                                </div>
                            </>
                        )
                    }
                </div>
            </div>
        </div>
     );
}

export default MyTrip;