import { Link } from "react-router-dom";
import Title from "../../../components/Title/Title";
import Image from "../../../components/Image/Image";
import MoneyFormatStaying from "../../../components/Staying/MoneyFormatStaying";
import { useState } from "react";
import { getLocale } from "../../../components/Locale/Locale";
import { useTranslation } from "react-i18next";
import ToolTip from "../../../components/ToolTip/ToolTip";
import Button from "../../../components/Buttons/Button";
import { SlOptionsVertical } from "react-icons/sl";
import routesConfig from "../../../configs/routesConfig";
import { format } from "date-fns";
import PropTypes from "prop-types";
import Icon from "../../../components/Icon/Icon";
import { FaRankingStar } from "react-icons/fa6";

function ItemTrip({ item }) {
  const [visible, setVisible] = useState(false);
  const locale = getLocale();
  const { t } = useTranslation();

  const items = [
    {
      name: t("MyTrip.items.item1"),
    },
    // {
    //     name: t("MyTrip.items.item2"),
    // }
  ];

  const handleShowOptions = () => {
    setVisible(!visible);
  };

  const currentDate = new Date();
  const nextDay = new Date(currentDate);
  nextDay.setDate(nextDay.getDate() + 1);

  return (
    <>
      <div className='flex flex-col gap-1'>
        <Title
          title={`${item?.hotel.hotelAddress}`}
          fontBold
          extraLarge9
          nowrap={false}
        />
        <Title
          title={`${format(new Date(item?.bookingDate), "dd LLLL yyyy", {
            locale,
          })}`}
          xxl
          nowrap={false}
        />
      </div>

      <div className="flex flex-col">
        <Link
          to={`${
            item?.payment.status === "STATUS_PAYMENT_PENDING"
              ? routesConfig.bookingconfirmation
              : routesConfig.mybooking
          }?bid=${item?.bookingId}`}
          className='relative z-0 mt-4 flex w-full rounded-md border border-gray-200 p-6 duration-200 shadow-[0_2px_8px_0_rgba(26,26,26,0.16)] hover:shadow-[0_2px_16px_0_rgba(26,26,26,0.24)]'
        >
          <div className='flex flex-row items-center w-full'>
            <div className='flex flex-row w-full'>
              <div className='flex flex-row items-start sm:items-center flex-1 gap-6'>
                <div className='min-w-[64px] min-h-[64px] max-w-[64px] max-h-[64px] 2md:w-[80px] 2md:h-[80px] 2md:max-w-[80px] 2md:max-h-[80px] opacity-60 bg-white'>
                  <Image
                    src={item?.hotel?.hotelImage}
                    className='w-full h-full object-cover'
                  />
                </div>
                <div className='flex flex-col gap-1 text-gray-500'>
                  <Title title={item?.hotel?.hotelName} fontBold xxl />
                  <div className='inline sm:flex flex-row items-center gap-1'>
                    <Title
                      title={`${format(
                        new Date(item?.bookingDate),
                        "dd LLLL yyyy",
                        { locale },
                      )}`}
                      xl
                      nowrap={false}
                    />
                    <Title
                      title={`${"·"}`}
                      xl
                      nowrap={false}
                      className='m-[0_4px] sm:m-0'
                    />
                    <Title
                      title={`${item?.address}`}
                      xl
                      nowrap={false}
                      className='2md:flex hidden'
                    />
                    <MoneyFormatStaying
                      price={item?.totalPrice}
                      decimalScale={0}
                      className='2md:hidden gap-1 text-[14px]'
                    />
                  </div>
                  {item?.payment.status === "STATUS_PAYMENT_PENDING" ? (
                    <Title title={t("MyTrip.confirm")} xl />
                  ) : (
                    <Title title={t("MyTrip.complete")} xl />
                  )}
                </div>
              </div>
  
              <div className='flex flex-row items-start'>
                <div className='flex flex-row items-center gap-2'>
                  <MoneyFormatStaying
                    price={item?.totalPrice}
                    decimalScale={0}
                    className='2md:flex hidden font-bold gap-1 text-[18px]'
                  />
                  <ToolTip
                    width={230}
                    placement='bottom-end'
                    isVisible={visible}
                    onClickOutside={() => handleShowOptions()}
                    typeToolTip='TippyHeadless'
                    interactive
                    items={
                      <div className='pt-2 pb-2 overflow-hidden rounded-md shadow-[0_2px_8px_0_rgba(26,26,26,0.16)]'>
                        <div className='flex flex-col w-full'>
                          {items.map((i, index) => (
                            <Link
                              to={`${routesConfig.hotelDetails}?id=${
                                item?.hotel?.hotelId
                              }&location=${
                                item?.hotel.city || "HCM"
                              }&checkin=${format(
                                new Date(),
                                "yyyy-MM-dd",
                              )}&checkout=${format(
                                nextDay,
                                "yyyy-MM-dd",
                              )}&group_adults=1&group_children=0&group_rooms=1`}
                              target='_blank'
                              key={index}
                              className='p-[10px_16px] hover:bg-gray-100 duration-300'
                            >
                              <Title title={i?.name} xl />
                            </Link>
                          ))}
                        </div>
                      </div>
                    }
                  >
                    <div>
                      <Button
                        icon={SlOptionsVertical}
                        size={16}
                        onClick={(e) => {
                          e.preventDefault();
                          handleShowOptions(item.bookingId);
                        }}
                        className='p-2 bg-transparent hover:bg-gray-100 duration-300 rounded-sm'
                        classButton='ml-1 mt-1 mb-1'
                      />
                    </div>
                  </ToolTip>
                </div>
              </div>
            </div>
          </div>
        </Link>
        <Link
          to={`${routesConfig.bookingreview.replace(":id", item.bookingId)}`}
          className='relative z-0 flex w-full rounded-md border border-gray-200 p-[12px_24px] duration-200 shadow-[0_2px_8px_0_rgba(26,26,26,0.16)] hover:shadow-[0_2px_16px_0_rgba(26,26,26,0.24)]'
        >
          <div className="flex flex-row gap-4 items-center">
              <Icon icon={FaRankingStar} size={32}/>
              <span className="text-[14px] capitalize">{t("Other.previews")} <strong className="text-secondary-200">{item.hotel.hotelName}</strong></span>
          </div>
        </Link>
      </div>
    </>
  );
}

ItemTrip.propTypes = {
  item: PropTypes.object.isRequired,
};

export default ItemTrip;
